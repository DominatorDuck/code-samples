using UnityEngine;
using System.Collections;

/*
	Player represents the ship the gamer controls
*/
public class Player : GunHolder
{
	//CONSTANTS
		//The player's gun has different levels (power), that are represented by gun indexes. From 0 to 6
		//Max value of the gun index, this is because I'll add more guns for the player later.
	private const int MAX_GUN_INDEX = 6;
	/*GUN ENERGY BAR*/
	//The player has an energy bar that dictates the gun index.
	//It goes from 0-100. When it reaches 100 we climb to a new gun level and if you get below 0 your gun index decrements 
	//This As the time passes this energy bar decreases and you can get it to increase by grabbing elements
	private const int ENERGYBAR_TIMED_DECREMENT = 2;//Every X time we decrement by this quantity
	private const int ENERGYBAR_INCREMENT = 60;//Every time you grab an element this is incremented

	//INSPECTOR VARIABLES (Unity editor)
	//The current life of our ship
	public int life;
	//The energy bar wich determines your current weapon from 0 to 100.
	public float EnergyBar_Value = 0;
	//CUrrent index of the weapon the player has
	[HideInInspector]
	public int currentGunIndex = 0;//starting at 0
	//The script that controls the game
	private GameController gameController;
	//to position the ship above the finger position
	private float Y_Offset = 1;
	//to indicate there is a new gun waiting to be set up - just acquired
	private bool hasNewGun = false;

	/*TO DEPLEAT THE BAR*/
	//Speed with which the energy bar will empty.
	private float EnergyBar_DepleatVel = 0.2f;//Each 0.2f the bar will decrease
	//We are storing the time on this variable to know when to decrement. No timers around here.
	private float EnergyBar_Accumulator = 0;


	//Construtor and super's constructor
	public Player() :base(0.5f, GunDirection.UP) {}

	//Set up initial conditions
	void Start()
	{
		gameController = GameObject.Find("GameController").GetComponent<GameController>();
		gameController.canBePausedNow = false;
		setCurrentShipColor();//As stored in memory
		setUpNewGun();
	}

	//If an enemy hits us they do damage
	public void Damage(int ammount)
	{
		if (gameController.isGamePlayRunning)//If we are in gameplay
		{
			life -= ammount;
			if (life <= 0)
				DeathSequence();
		}
	}

	//All the actions we need to do when we die.
	private void DeathSequence()
	{
		Destroy(gameObject);
		Instantiate(Resources.Load("Prefabs/Explosions/explosion_player"), transform.position, transform.rotation);
		gameController.resetGameAfterTime(1);
	}

	void Update()
	{
		if (gameController.isGamePlayRunning)
		{
			/*INPUT*/
			if (Input.GetButtonDown("Fire1")) {
				if (gun != null)
					gun.Enable();
				if (!gameController.canBePausedNow)
					gameController.canBePausedNow = true;
			}
			if (!gameController.uiController.isPaused)
			{
				if (Input.GetButtonUp("Fire1")) {
					if (gun != null)
						gun.Disable();
					if (gameController.canBePausedNow)
						gameController.uiController.pause();//Game is unpaused in pauseScreenController
				}
				if (Input.GetButton("Fire1")) {//right click of mouse or touch screen
						//We position the ship where the finger/mouse is
					Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					transform.position = new Vector3(clickPosition.x, clickPosition.y + Y_Offset, transform.position.z);
				}
				/*HAS NEW GUN*/
				if (hasNewGun) {
					if (gameObject.GetComponent<AbstractGun>() != null) {
						hasNewGun = false;
						setUpNewGun();
					}
				}
				/*SHOWING ENERGY BAR VALUE*/
				if (Time.time > EnergyBar_Accumulator) {
					EnergyBar_Accumulator = Time.time + EnergyBar_DepleatVel;
					if (currentGunIndex == 0)
						decrementEnergyBar(ENERGYBAR_TIMED_DECREMENT);
					else
						decrementEnergyBar(ENERGYBAR_TIMED_DECREMENT + currentGunIndex);
				}
			}
		}
	}

	//Every time we need to decrease the energy bar we call this
	private void decrementEnergyBar(int ammount)
	{
		if (EnergyBar_Value == 0 && currentGunIndex == 0)//We do not have a gun, what is there to take?
			return;
		EnergyBar_Value -= ammount;
		if (EnergyBar_Value < 0) {
			if (currentGunIndex == 1) {//We are gonna go to zero
				gameController.requestCollectable();//We request a collectable because we're aout of energy
				currentGunIndex = 0;
				EnergyBar_Value = 0;
				setGunByIndex();
			}
			else {
				currentGunIndex--;
				EnergyBar_Value += 100;
				setGunByIndex();
			}
		}
	}

	//Whenever we pick an incorrect collectable/element
	public void decrementEnergyBar()
	{
		decrementEnergyBar(ENERGYBAR_INCREMENT);//We decrement our energy as a penalty
	}


	//We give the ship it's color. Depending on what the  user has chosen
	public void setCurrentShipColor()
	{
		GetComponent<SpriteRenderer>().sprite = Resources.Load("Sprites/Player/" + gameController.metaGame.currentShipColor, typeof(Sprite)) as Sprite;
	}

	//Whenever we pick a correct collectable/element
	public void incrementEnergyBar()
	{
		//We increment the energy bar
		EnergyBar_Value += ENERGYBAR_INCREMENT;
		//What does that increment mean? 
		if (currentGunIndex == 0) {//We had no weapon
			if (currentGunIndex < MAX_GUN_INDEX)
			{
				currentGunIndex++;
				setGunByIndex();
			}
		}
		else {
			if (EnergyBar_Value > 100) {//WE GET NEW GUN!
				if (currentGunIndex != MAX_GUN_INDEX)
					EnergyBar_Value -= 100;
				else
					EnergyBar_Value = 100;//clamped if max level
				if (currentGunIndex < MAX_GUN_INDEX)
				{
					currentGunIndex++;
					setGunByIndex();
				}
			}
		}
	}

	//According to the current gun index we add a new gun to this GameObject
	private void setGunByIndex()
	{
		Destroy(gameObject.GetComponent<AbstractGun>());
		switch (currentGunIndex) {
		case 0:
			gameObject.AddComponent<OrangeBase>();
			break;
		case 1:
			gameObject.AddComponent<OrangeCannon_I>();
			break;
		case 2:
			gameObject.AddComponent<OrangeCannon_II>();
			break;
		case 3:
			gameObject.AddComponent<OrangeCannon_III>();
			break;
		case 4:
			gameObject.AddComponent<BrownCannon_I>();
			break;
		case 5:
			gameObject.AddComponent<BrownCannon_II>();
			break;
		case 6:
			gameObject.AddComponent<OrangeCannon_V>();
			break;
		}
		hasNewGun = true;
	}

	/*Each time the player changes weapon it has to be set*/
	private void setUpNewGun()
	{
		gun = gameObject.GetComponent<AbstractGun>();//We find the weapon script
		gun.gunDirection = this.gunDirection;
		gun.YOffset = gunYOffset;
		if (Input.GetButton("Fire1") && gameController.isGamePlayRunning)
			gun.Enable();
	}

}