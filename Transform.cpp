//The class that has all the transformations needed to position all objects
//Note: matrices in GLM are column major: you mus specify from top to bottom starting with the first column.
#include "Transform.h"
#include <iostream>

using namespace std;

//Rotates a point some degreees (input is in degrees, not radians) around a vector in 3D space (not homogenous, obviously)
//returns the rotation matrix to be applied
mat3 Transform::rotate(const float degrees, const vec3& axis) 
{
	//USING THE RODRIGUES' FORMULA FOR ROTATIONS IN 3D
	//vars for quick access to stuff and readeability
	//we first normalize the axis.
	vec3 _axis= glm::normalize(axis);
	float angleInRadians=glm::radians(degrees);//Radians, the GLM formula asks for that
	float x=_axis.x;
	float y=_axis.y;
	float z=_axis.z;
	//Applying the matrix multiplications
	mat3 matriz1=mat3(cos(angleInRadians),0,0,0,cos(angleInRadians),0,0,0,cos(angleInRadians));
	mat3 matriz2=mat3(x*x,x*y,x*z,x*y,y*y,y*z,x*z,y*z,z*z)*(1-cos(angleInRadians));
	mat3 matriz3=mat3(0,z,-y,-z,0,x,y,-x,0)*(sin(angleInRadians));
	//printMatrix3(matriz1+matriz2+matriz3);
	return matriz1+matriz2+matriz3;
}

//Scales a point irregularly, with each magnitude specified
//returns the scale matrix to be applied
mat4 Transform::scale(const float &sx, const float &sy, const float &sz) 
{
	//scale should be done by this transformation matrix
    //[SX 0  0  0]
	//[0 SY  0  0]
	//[0  0 SZ  0]
	//[0  0  0  1]
	mat4 scaleMatrix4= mat4(sx,0,0,0,0,sy,0,0,0,0,sz,0,0,0,0,1);
	return scaleMatrix4;
}

//Translates point in 3D space
//returns the translate matrix to be applied
mat4 Transform::translate(const float &tx, const float &ty, const float &tz) 
{
   	//translation should be done by this matrix
    //[1  0  0 tx]
	//[0  1  0 ty]
	//[0  0  1 tz]
	//[0  0  0  1]
	mat4 translateMatrix4=mat4(1,0,0,0,0,1,0,0,0,0,1,0,tx,ty,tz,1);
	return translateMatrix4;
}

Transform::Transform()
{

}

Transform::~Transform()
{

}

//Debug helper function
void Transform::printMatrix3(const mat3 matriz)
{
	printf("\nMatrix3x3: \n");
	for(int x=0;x<3;x++)
	{
		printf("[");
		for(int y=0;y<3;y++)
		{
			printf("%.2f, ",matriz[y][x]);
		}
		printf("]\n");
	}
}

//Debug helper function
void Transform::printMatrix4(const mat4 matriz)
{
	printf("\nMatrix4x4: \n");
	for(int x=0;x<4;x++)
	{
		printf("[");
		for(int y=0;y<4;y++)
		{
			printf("%.2f, ",matriz[y][x]);
		}
		printf("]\n");
	}
}
