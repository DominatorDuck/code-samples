package  
{
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import org.flixel.plugin.photonstorm.BaseTypes.*;
	/**
	 * @author Megaton
	 */

	//This class represents the gun for the player. (It potentially may be used by an enemy)
	//The gun is a object that simply spawns bullets in a given direction and following a certain trajectory, computed here.
	//Every time the gun empties the gun is refilled with a random effect and trajectory for the bullets.
	public class LazerGun extends FlxWeapon 
	{
		//private members
		//Max bullets the gun can store
		private const clipSize:uint=15;
		//A reference to the parent, we may be interested in it's position and it's facing direction to spawn bullets.
		private var parent:*;

		//public members
		//current number of bullets in the gun
		public var cartridge:uint;
		//The current effect of the gun, wich in turn affects the effect and color of the fired bullets
		public var current_effect:String;
		//The current trajectory the fired bullets will inherit
		public var current_trayectory:String;
		
		//We only need the parent(player or enemy) in the constructor.
		public function LazerGun(parent:*) 
		{
			super("lazer", parent, "x", "y");
			super.bounds = new FlxRect(0, -99999, 99999, 99999);//Outside this square bullets dissapear. Level's dimensions are unknown.
			this.parent = parent;
			makeImageBullet(10, Assets.bullet_normal);
			setBulletOffset(105, 44);//Bullets will spawn from the parent's x and y using these offsets for x and y. 
			setFireRate(500);
			setBulletLifeSpan(2500);//Bullets die after 2.5 seconds
			setPostFireCallback(postFair);
			setBulletDirection(FlxWeapon.BULLET_RIGHT, 500);
			reload();//Randomizes cartridge size, bullets trajectory and bullets effect.
		}
		
		//After the gun has fired a bullet this function gets called. It's set as a callback in the constructor
		public function postFair():void
		{
			if(cartridge>0)//If the bullet is not empty, reduce current bullets
				cartridge--
			else
				reload();
			//After we fire a bullet we need to alter the bullet's properties:
			Level1(FlxG.state).hud.setBullets(cartridge);
			this.currentBullet.currentEffect = current_effect;
			this.currentBullet.currentTrayectory = current_trayectory;
			this.currentBullet.startingPos = new FlxPoint(parent.x + 105, parent.y + 44);
			this.currentBullet.loadGraphic(setGraphicEffect(),false,false,21,20)//function to set graphic given the bullet effect
		}
		
		//Each bullet follows a trajectory. Trajectories are approximations to functions: fourier transforms. 
		//Bullets ask this function for their location in y, given their own trajectory and their position in x.
		//We take into account the position the bullet was fired as the origin of their coordinate system in the calculation 
		public function setTrayectory(bul:Bullet):int
		{
			//let's get the bullet's real X, transforming their world X into their own coordiante system.
			var equis:int = (Agent(parent).facing==FlxObject.RIGHT)?bul.x - bul.startingPos.x:bul.x - bul.startingPos.x+Agent(parent).width;
			//Now choose between our repetoire of trajectories
			switch(bul.currentTrayectory)
			{
				case "sen":
					return -Math.sin(equis/(20*Math.PI)) * 100;
				case "exp":
					return -Math.pow(equis, 2) / 800;
				case "ABSsen":
					return -Math.abs(-Math.sin(equis/(20*Math.PI)) * 100)
				case "-ABSsen":
					return Math.abs(-Math.sin(equis/(20*Math.PI)) * 100)
				case "x":
					return -equis;
				case "FOURsquare":
					return (-100)*((1/2)+((2/Math.PI)*Math.sin(equis/(30*Math.PI)))+((2/(Math.PI*3))*Math.sin(3*equis/(30*Math.PI)))+((2/(Math.PI*5))*Math.sin(5*equis/(30*Math.PI))))
				case "FOURsquare2":
					return (-50)*((1/2)+((2/2)*Math.sin(equis/(50*Math.PI)))+((2/2)*Math.sin(3*equis/(50*Math.PI)))+((2/2)*Math.sin(5*equis/(50*Math.PI))))
				case "FOURsawtooth":
					return (-50)*(((2/1)*Math.sin(1*equis/(30*Math.PI)))-((2/2)*Math.sin(2*equis/(30*Math.PI)))+((2/3)*Math.sin(3*equis/(30*Math.PI)))-((2/4)*Math.sin(4*equis/(30*Math.PI)))+((2/5)*Math.sin(5*equis/(30*Math.PI))))
				case "FOURx":
					return ( -50) * (2 * Math.sin(equis/(30*Math.PI)) + Math.sin(2 * equis/(30*Math.PI)) - (2 / 3) * Math.sin(3 * equis/(30*Math.PI)))
				case "FOURexp":
					return -Math.abs((50)*(Math.PI*((1/10)*(100*(1-Math.cos(equis/50)+Math.sin(equis/50))*Math.sin(Math.PI/50)))))
			}
			return equis;
		}
		
		//Given the current effect we choose the graphic that will be used while firing by now.
		private function setGraphicEffect():Class
		{
			switch(current_effect)
			{
				case "normal":
					return Assets.bullet_normal
				case "fire":
					return Assets.bullet_fire
				case "acid":
					return Assets.bullet_acid
				case "ice":
					return Assets.bullet_ice
				case "electricity":
					return Assets.bullet_electricity
				case "explosion":
					return Assets.bullet_explosion
				case "transform":
					return Assets.bullet_transform
				case "acid":
					return Assets.bullet_acid
				case "fire":
					return Assets.bullet_fire
				case "normal":
					return Assets.bullet_normal
			}
			return Assets.bullet_normal;
		}
		
		//Each time the lazer empties we need to refill the weapon using random effect, clip size and trajectory
		public function reload():void
		{
			cartridge = addBullets();//Refill clip, please
			Level1(FlxG.state).hud.setBullets(cartridge);//We alter the HUD to let the user know which effect, clip size and trajectory he has.
			current_trayectory = shuffleTrayectory();
			current_effect = shuffleEffect();
			Level1(FlxG.state).hud.setTrayectory(current_trayectory);
			Level1(FlxG.state).hud.setEffect(current_effect);
		}
		
		//It returns a random trajectory
		private function shuffleTrayectory():String
		{
			var num:uint;
			num = Math.random() * 11;
			switch(num)
			{
				case 1:
					return "sen"
				case 2:
					return "exp"
				case 3:
					return "ABSsen"
				case 4:
					return "FOURexp"
				case 5:
					return "x"
				case 6:
					return "FOURsquare"
				case 7:
					return "FOURsquare2"
				case 8:
					return "FOURsawtooth"
				case 9:
					return "FOURx"
				case 10:
					return "-ABSsen"
			}
			return "sen";
		}
		
		//Returns a rendom effect for the bullet
		private function shuffleEffect():String
		{
			var num:uint;
			num = Math.random() * 10;
			switch(num)
			{
				case 1:
					return "normal"
				case 2:
					return "fire"
				case 3:
					return "acid"
				case 4:
					return "ice"
				case 5:
					return "electricity"
				case 6:
					return "explosion"
				case 7:
					return "transform"
				case 8:
					return "acid"
				case 9:
					return "fire"
				case 10:
					return "normal"
			}
			return "normal"
		}
		
		//Randomly refill the gun with bullets
		private function addBullets():uint
		{
			var num:uint = Math.random() * clipSize;
			if (num <= 0)
				return 1;
			return (num > clipSize)?clipSize:num;
		}
		
		override public function update():void 
		{
			super.update();
		}
		
	}

}