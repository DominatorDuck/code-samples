﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
	The game levels are comprised by a number of chunks. Each time the player goes through a spawn trigger, the Spawner spawns a chunk.
	This is spawned on the fly because the game is an infinite runner
*/
public class Spawner : MonoBehaviour {
	//public, set in the inspector: Unity editor or Unity's GUI 
	//We must spawn an initial platform for our hobbo to spawn in, don't we?
	public GameObject initialPlatform;
	//A collection of all possible chunks, each time we must spawn a chunk we choose one.
	public List<GameObject> chunksCollection;
	//We'll get properties such as the chunks' position based on the last spawned chunk. We need the reference
	public GameObject lastObstacle;
	//private
	//The x, y and z of the next chunk we'll spawn
	private float chunkY,chunkZ;
	private float nextPlatformX;
	// Use this for initialization.
	void Start () 
	{
		//We have to start with an initial plaftorm wich is set in the inspector
		chunkY = initialPlatform.transform.position.y;
		chunkZ = initialPlatform.transform.position.z;
		//We must start a second chunk to kick start the game, and not get an empty background
		lastObstacle=initialPlatform;
		spawnChunk();
	}


	//Whenever the player goes through a trigger we spawn a new chunk
	public void spawnChunk()
	{
		if(canCreateNewChunk())//If we can create a chunk...
		{
			GameObject chunkToSpawn = chunksCollection[Random.Range(0,chunksCollection.Count)];//random chunk
			nextPlatformX = lastObstacle.renderer.bounds.max.x+chunkToSpawn.renderer.bounds.extents.x;//we calculate X, given last chunk's dimensions
			lastObstacle =Instantiate(chunkToSpawn,new Vector3(nextPlatformX,chunkY,chunkZ),Quaternion.identity) as GameObject;//Spawning a chunk
		}
	}

	//We prevent the game from spawning too many chunks, so if we have many invisible platforms (ahead of the player) we do not spawn
	private bool canCreateNewChunk()
	{
		int numberOfInvisiblePlatforms=0;
		//We find the platforms in the scene
		GameObject[] platforms = GameObject.FindGameObjectsWithTag("Platform");
		foreach (GameObject platform in platforms) 
		{
			if(!platform.renderer.IsVisibleFrom(Camera.main))
			{//Now we count he platforms that are invisible to us
				numberOfInvisiblePlatforms++;
			}
		}
		if(numberOfInvisiblePlatforms>2)return false;
		else return true;
	}
}
